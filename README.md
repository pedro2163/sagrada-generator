# Sagrada generator

This script generate card for your PnP projects.
The original idea came from (this bgg post)[https://boardgamegeek.com/filepage/219948/sagrada-card-generator] and I decide to migrate de PowerShell script to python script.

## Execution

```
git clone https://gitlab.com/jirsis/sagrada-generator.git
cd sagrada-generator
pip3 install virtualenv
virtualenv env
source env/bin/activate
pip install -r requirements.txt
python sagrada_generator.py
```

## Modification

You can modify ```card.txt``` with these format and you can generate your own cards.

